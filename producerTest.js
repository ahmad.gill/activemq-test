const getProducer = require("./node_modules/@adnoc-dist/adnoc-event-module/producers")
const TestEvent = require('./node_modules/@adnoc-dist/adnoc-event-module/events/test')
const getLogger = require("./node_modules/@adnoc-dist/adnoc-event-module/logger")
global.log = getLogger();

const name = "dev"


const dev = {
    "KAFKA_CLIENT_LOGLEVEL": 2,
    "NAME": "dev",
    "BROKERS_LIST": "b-2.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098,b-1.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098,b-3.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098",
    "TOPIC_PARITION_COUNT": 2,
    "TOPIC_REPLICATION_FACTOR": 2,
    "KAFKA_CLIENT_ID": "data-migration-app",
    "ACTIVEMQ" :{
        "host" : ["b-2c74c986-6343-4f5c-9be0-f713dac13dfc-1.mq.eu-west-1.amazonaws.com"],
        "port" : [5671],
        "username" : "",
        "password" : ""
    }
}
async function main() {

    const producer = await getProducer(dev);
    producer.produceEvent(new TestEvent({
        "key1": "test1",
        "key2": 23,
        "key3": {
            "nestedkey1": "test1"
        }
    }))
}

main().then(() => {
    console.log("successfully exeecuted")
})