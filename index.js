var container = require('rhea');
const { v4: uuidv4 } = require('uuid');
const sqlite3 = require('sqlite3').verbose();
const dbPath = './example.db';
const db = new sqlite3.Database(dbPath);
db.run(`CREATE TABLE IF NOT EXISTS messages (
    id INTEGER PRIMARY KEY,
    messageId INTEGER,
    delay INTEGER,
    status INTEGER,
    processTimeStamp INTEGER
  )`);


let messageProcessedCount =0, messageSentCount =0;
function generateRandomNumber() {
    return Math.floor(Math.random() * (500000 - 10000 )) + 10000;
}

function saveInDb(db, data){
    db.run(`INSERT INTO messages (messageId, delay, status) VALUES (?, ?, ?)`, [data.messageId, data.delay, 0], function(err) {
        if (err) {
          return console.error(err.message);
        }
        console.log(`A row has been inserted with id ${this.lastID}`);
      });
}
// saveInDb(db, {
//     messageId : 1, 
//     delay : 1
// })
function updateSuccessfulMessageInDB(db, data){
    db.run(`UPDATE messages SET status = 1, processTimeStamp=? WHERE messageId = ?`, [Date.now(), data.messageId], function(err) {
        if (err) {
          return console.error(err.message);
        }
        console.log(`Number of rows updated: ${this.changes}`);
      });
}
// updateSuccessfulMessageInDB(db, {
//     messageId : 1
// })
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
container.on('message', function (context) {
    console.log(context.message.body)
    console.log(`${context.message.body.index} Time to process ${Date.now() - context.message.body.eventTime} - delay ${context.message.body.delay}`);
    messageProcessedCount = messageProcessedCount + 1;
    updateSuccessfulMessageInDB(db, {
        messageId : context.message.body.messageId
    })
    console.log(`messageProcessedCount : ${messageProcessedCount}`)
    // context.connection.close();
});
container.once('sendable', async function (context) {
    console.log("test")
    for (let i=0; i < 5; i++) {
        const delay = generateRandomNumber();
        
        if(context.sender.sendable()){
           
            let result = context.sender.send({ body: { msg: 'Hello World!', delay: delay, eventTime: Date.now() , messageId : i}, message_annotations: { 'x-opt-delivery-delay': delay } });
            console.log('result', result)
            saveInDb(db, {
                messageId : i, 
                delay : delay
            })
            messageSentCount = messageSentCount + 1;
            console.log(`messageSentCount : ${messageSentCount}`)
        }else{
            console.log("Waiting")
            await sleep(5000)
            i = i-1;
        }

    }
});

var connection = container.connect({
    host:'localhost',
     'port':5672,
     username:'admin',
     password:'admin',
    container_id: uuidv4(),
    id: uuidv4(),
    transport: 'tls', 
    reconnect: true,
});
connection.open_receiver('examples');
connection.open_sender('examples');
console.log('--------123')