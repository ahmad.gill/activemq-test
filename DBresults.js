const sqlite3 = require('sqlite3').verbose();
const dbPath = './example.db';
const db = new sqlite3.Database(dbPath);

// Define the SQL SELECT query
const selectDoneQuery = 'SELECT COUNT(*) FROM messages where status =1';
const selectNotDoneQuery = 'SELECT COUNT(*) FROM messages where status =0';

// Execute the SELECT query to retrieve all rows
db.all(selectDoneQuery, [], (err, rows) => {
    if (err) {
        console.error(err.message);
    } else {
        // Process the rows returned by the query
        rows.forEach(row => {
            console.log("selectDoneQuery", row); // Log each row to the console
        });
    }
});


db.all(selectNotDoneQuery, [], (err, rows) => {
  if (err) {
      console.error(err.message);
  } else {
      // Process the rows returned by the query
      rows.forEach(row => {
          console.log("selectNotDoneQuery", row); // Log each row to the console
      });
  }
});
// Close the database connection after query execution
db.close();
