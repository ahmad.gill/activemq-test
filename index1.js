const amqlib = require('amqplib');

const connectionURL = 'amqp://admin:admin@localhost'; // Update with your ActiveMQ server details

amqlib.connect(connectionURL)
  .then((connection) => {
    // Do something with the connection
    console.log('Connected to ActiveMQ');
    connection.close(); // Close the connection when done
  })
  .catch((error) => {
    console.error('Error connecting to ActiveMQ:', error.message);
  });
