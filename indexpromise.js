// var container = require('rhea');
const {
  Connection, Sender, EventContext, Message, ConnectionOptions, Delivery, SenderOptions, ReceiverEvents
} = require("rhea-promise");
const { v4: uuidv4 } = require('uuid');
/*
const sqlite3 = require('sqlite3').verbose();
const dbPath = './example.db';
const db = new sqlite3.Database(dbPath);
db.run(`CREATE TABLE IF NOT EXISTS messages (
    id INTEGER PRIMARY KEY,
    messageId INTEGER,
    delay INTEGER,
    status INTEGER,
    processTimeStamp INTEGER
  )`);


let messageProcessedCount = 0, messageSentCount = 0;
function generateRandomNumber() {
  return Math.floor(Math.random() * (500000 - 10000)) + 10000;
}

function saveInDb(db, data) {
  db.run(`INSERT INTO messages (messageId, delay, status) VALUES (?, ?, ?)`, [data.messageId, data.delay, 0], function (err) {
    if (err) {
      return console.error(err.message);
    }
    console.log(`A row has been inserted with id ${this.lastID}`);
  });
}
// saveInDb(db, {
//     messageId : 1, 
//     delay : 1
// })
function updateSuccessfulMessageInDB(db, data) {
  db.run(`UPDATE messages SET status = 1, processTimeStamp=? WHERE messageId = ?`, [Date.now(), data.messageId], function (err) {
    if (err) {
      return console.error(err.message);
    }
    console.log(`Number of rows updated: ${this.changes}`);
  });
}
// updateSuccessfulMessageInDB(db, {
//     messageId : 1
// })
*/
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
// container.on('message', function (context) {
//     console.log(context.message.body)
//     console.log(`${context.message.body.index} Time to process ${Date.now() - context.message.body.eventTime} - delay ${context.message.body.delay}`);
//     messageProcessedCount = messageProcessedCount + 1;
//     updateSuccessfulMessageInDB(db, {
//         messageId : context.message.body.messageId
//     })
//     console.log(`messageProcessedCount : ${messageProcessedCount}`)
//     // context.connection.close();
// });
// container.once('sendable', async function (context) {
//     console.log("test")
//     for (let i=0; i < 5; i++) {
//         const delay = generateRandomNumber();

//         if(context.sender.sendable()){

//             let result = context.sender.send({ body: { msg: 'Hello World!', delay: delay, eventTime: Date.now() , messageId : i}, message_annotations: { 'x-opt-delivery-delay': delay } });
//             console.log('result', result)
//             saveInDb(db, {
//                 messageId : i, 
//                 delay : delay
//             })
//             messageSentCount = messageSentCount + 1;
//             console.log(`messageSentCount : ${messageSentCount}`)
//         }else{
//             console.log("Waiting")
//             await sleep(5000)
//             i = i-1;
//         }

//     }
// });


// var connection = container.connect({
//     host:'localhost',
//      'port':5672,
//      username:'admin',
//      password:'admin',
//     container_id: uuidv4(),
//     id: uuidv4(),
//     // transport: 'tls', 
//     reconnect: true,
// });
// connection.open_receiver('examples');
// connection.open_sender('examples');
// console.log('--------123')

let messageProcessedCount = 0, messageSentCount = 0;
function generateRandomNumber() {
  return Math.floor(Math.random() * (500000 - 10000)) + 10000;
}

async function connectToBroker() {
  let connectionAttempts = 0;
  return new Promise(async (resolve, reject) => {
    const ports = [
      5672,
      5672
    ];
    const hosts = [
        "b-a812c7b7-8ecf-44f4-adaf-f56df4a227b7-1.mq.eu-west-1.amazonaws.com",
        "b-a812c7b7-8ecf-44f4-adaf-f56df4a227b7-2.mq.eu-west-1.amazonaws.com"
      ]
    
    const connectionOptions = {
      //host: 'b-2c74c986-6343-4f5c-9be0-f713dac13dfc-1.mq.eu-west-1.amazonaws.com',
      //'port': 5671,
      // host: 'localhost',
      // port: 5672,
  
      username: 'admin',
      password: 'admin',
      container_id: uuidv4(),
      id: uuidv4(),
      // transport: 'tls',
      reconnect: true,
      connection_details: () => { // reason for connection detail is to handle Failover support
        connectionAttempts = connectionAttempts + 1;
        console.log(`Connecting to ActiveMq cluster at ${hosts[connectionAttempts % hosts.length]}`);
        
        return {
          port: ports.length ? ports[connectionAttempts % ports.length] : ports,
          host: hosts.length ? hosts[connectionAttempts % hosts.length] : hosts,
          transport: 'tls'
        };
      }
    }
      const connection = new Connection(connectionOptions);

      connection.on('connection_open', (context) => {
          console.log('Connection opened or re-established.');
          resolve(context.connection); // Resolve the Promise with the connection
      });

      connection.on('connection_error', (context) => {
          console.error('Connection error:', context.error);
          reject(context.error); // Reject the Promise with the error
      });

      connection.on('connection_close', (context) => {
        console.error('Connection close1:', context.error);  
      });

      connection.on('disconnected', (context) => {
        console.log('Disconnected from the server');
        
        // Attempt to reconnect
        // connection.reconnect();
    });

   

    connection.on('error', (context) => {
      console.log('error from the server');
      
      // Attempt to reconnect
      connection.reconnect();
  });
      console.log("testoing")
       connection.open().catch(()=>{
        console.log('catching exception')
      }); // Open the connection
  });
}

async function start() {
  // const ports = [
  //   5671,
  //   5671
  // ];
  // const hosts = [
  //   "b-a812c7b7-8ecf-44f4-adaf-f56df4a227b7-1.mq.eu-west-1.amazonaws.com",
  //   "b-a812c7b7-8ecf-44f4-adaf-f56df4a227b7-2.mq.eu-west-1.amazonaws.com"
  // ]

  const ports = [
    5671,
    5671
  ];
  const hosts = [
    'localhost',
    'localhost'
  ]
  let connectionAttempts = 0;

  let senderAddress = "address";

  const connection = await connectToBroker();
  console.log("Connection called")
  // const connection = new Connection({
  //   //host: 'b-2c74c986-6343-4f5c-9be0-f713dac13dfc-1.mq.eu-west-1.amazonaws.com',
  //   //'port': 5671,
  //   // host: 'localhost',
  //   // port: 5672,

  //   username: 'admin',
  //   password: 'admin',
  //   container_id: uuidv4(),
  //   id: uuidv4(),
  //   // transport: 'tls',
  //   reconnect: true,
  //   reconnect_limit: 3 ,  
  //   connection_details: () => { // reason for connection detail is to handle Failover support
  //     connectionAttempts = connectionAttempts + 1;
  //     console.log("connection again");
  //     console.log(hosts[connectionAttempts % hosts.length])
  //     return {
  //       port: ports.length ? ports[connectionAttempts % ports.length] : ports,
  //       host: hosts.length ? hosts[connectionAttempts % hosts.length] : hosts,
  //       // transport: 'tls'
  //     };
  //   }
  // });

  // connection.on('connection_open', (context) => {
  //   console.log('Connection opened or re-established.');
  // });

  // connection.on('connection_error', (context) => {
  //   console.error('Connection error:', context.error);  
  // });

  // connection.on('connection_close', (context) => {
  //   console.error('Connection close1:', context.error);  
  // });

  const senderName = senderAddress;
  const senderOptions = {
    name: senderName,
    target: {
      address: senderAddress
    },
    sendTimeoutInSeconds: 10,
    onError: (context) => {
      const senderError = context.sender && context.sender.error;
      if (senderError) {
        console.log(">>>>> [%s] An error occurred for sender '%s': %O.",
          connection.id, senderName, senderError);
      }
    },
    onSessionError: (context) => {
      const sessionError = context.session && context.session.error;
      if (sessionError) {
        console.log(">>>>> [%s] An error occurred for session of sender '%s': %O.",
          connection.id, senderName, sessionError);
      }
    }
  };

  if (connection.isOpen()) {
    console.log("Connection opened1")
  } else {
    console.log("Connection not opened")
  }
  // connection.open().then(()=>{
  //   console.log("Connection Successful")
  // }).catch(()=>{
  //   console.log("Connection Failed---")
  // });


  // if (connection.isOpen()) {
  //   console.log("Connection opened1")
  // } else {
  //   await sleep(10000)
  // }
  //receiver
  const receiverName = senderAddress;
  const receiverOptions = {
    name: receiverName,
    source: {
      address: senderAddress
    },
    onSessionError: (context) => {
      const sessionError = context.session && context.session.error;
      if (sessionError) {
        console.log(">>>>> [%s] An error occurred for session of receiver '%s': %O.",
          connection.id, receiverName, sessionError);
      }
    }
  };

  const receiver = await connection.createReceiver(receiverOptions);

  receiver.on(ReceiverEvents.message, (context) => {
    // console.log("Received message: %O", context.message);
    console.log(`${context.message.body.index} Time to process ${Date.now() - context.message.body.eventTime} - delay ${context.message.body.delay}`);
    messageProcessedCount = messageProcessedCount + 1;
    console.log(`messageProcessedCount : ${messageProcessedCount}`)
    console.log(">>>>> [%s] message processed  for receiver '%s'",
      connection.id, receiverName);
  });
  receiver.on(ReceiverEvents.receiverError, (context) => {
    const receiverError = context.receiver && context.receiver.error;
    if (receiverError) {
      console.log(">>>>> [%s] An error occurred for receiver '%s': %O.",
        connection.id, receiverName, receiverError);
    }
  });

  //sender
  const sender = await connection.createAwaitableSender(senderOptions);

  for (let i = 0; i < 5; i++) {
    const delay = generateRandomNumber();
    console.log('coming to send')
    const delivery = await sender.send({ body: { msg: 'Hello World!', delay: delay, eventTime: Date.now(), messageId: i }, message_annotations: { 'x-opt-delivery-delay': delay } });
    console.log(">>>>>[%s] Delivery id: %d, settled: %s",
      connection.id,
      delivery.id,
      delivery.settled);
    messageSentCount = messageSentCount + 1;
    console.log(`messageSentCount : ${messageSentCount}`)

  }








}

start().catch((err) => console.log(err));

process.on('SIGINT', async () => {
  console.log('Shutting down...');
 
  process.exit(0);
});