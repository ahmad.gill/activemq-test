
const TestEvent = require('./node_modules/@adnoc-dist/adnoc-event-module/events/test')
const EquipmentInfoChanged = require('./node_modules/@adnoc-dist/adnoc-event-module/events/washAndGo/EquipmentInfoChanged')
const getLogger = require("./node_modules/@adnoc-dist/adnoc-event-module/logger")
const getConsumer = require("./node_modules/@adnoc-dist/adnoc-event-module/consumers")



global.log = getLogger();
const listener = require("./node_modules/@adnoc-dist/adnoc-event-module/listener")
process.env.NAME = 'dev'


class TestListener extends listener.MessageListener{
    constructor( options){
        super(EquipmentInfoChanged, options)   
    }

     sleep(ms) {
        console.log("this is here ")
        return new Promise(resolve => setTimeout(resolve, ms));
      }

    async run(event){
        console.log("EVENT ---", event)
    }

    fallBackRun(event){
        console.log("FallbackRun - EVENT2 ---", event)
        // throw new Error("Something went wrong")
    }
}

class TestListener1 extends listener.MessageListener{
    constructor( options){
        super(TestEvent, options)   
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }

    run(event){
        console.log("EVENT1 ---", event)
    }

    fallBackRun(event){
        console.log("FallbackRun - EVENT2 ---", event)
        // throw new Error("Something went wrong")
    }
}

class TestListener2 extends listener.MessageListener{
    constructor( options){
        super(TestEvent, options)
        this.count = 0   
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }
    run(event){
        //console.log("EVENT2 ---", event)
        // this.count = this.count + 1;
        // if(this.count %2==0){
        throw new Error("Something went wrong")
        // }
    } 

    fallBackRun(event){
        console.log("FallbackRun - EVENT2 ---", event)
        // throw new Error("Something went wrong")
    }
}



// consumer.listenToEvent(TestEvent.name, (event)=>{
//     console.log("EVENT ---", event)
//     // throw new Error("Throwing Error")
// },{
//     retry : false
// })


// consumer.listenToEvent(new TestListener({
//     retry : false
// }))

// consumer.listenToEvent(new TestListener1({
//     retry : false
// }))

const dev = {
    "KAFKA_CLIENT_LOGLEVEL": 2,
    "NAME": "dev",
    "BROKERS_LIST": "b-2.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098,b-1.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098,b-3.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098",
    "TOPIC_PARITION_COUNT": 2,
    "TOPIC_REPLICATION_FACTOR": 2,
    "KAFKA_CLIENT_ID": "data-migration-app",
    "ACTIVEMQ" :{
        "host" : ["b-2c74c986-6343-4f5c-9be0-f713dac13dfc-1.mq.eu-west-1.amazonaws.com"],
        "port" : [5671],
        "username" : "mqadmin",
        "password" : "BJm.6bf/72d8MgQ"
    }
}

const sampleFunction = async ()=>{
    const consumer = getConsumer( {
        group_id : "test"
    }, dev);
    console.log(consumer.events)
    await consumer.listenToEvent(new TestListener2({
        retry : true,
        haveFailureFallBack : true,
        delayInterval : 14000,
        maxRetries : 2,
        initialDelayInterval : 1000
    }))

    await consumer.listenToEvent(new TestListener({
        retry : true,
        haveFailureFallBack : true,
        delayInterval : 14000,
        maxRetries : 2,
        initialDelayInterval : 1000
    }))
    
    consumer.run();
}

sampleFunction().then(()=>{
    console.log("successfully exeecuted")
})